## GDS Summer Project 2016
### Prototype 1

This repository is not necessarily going to be used as the official repository
for [GDS Summer Project](gdsufrgs.com/summer). This is merely an intent to test good practices to be used
with the real project, including

- [Git LFS](http://gdsufrgs.com/unreal4-git/) tracking policies
- [Gitflow Workflow](http://nvie.com/posts/a-successful-git-branching-model/)
branching model
- GitLab integration with Asana
